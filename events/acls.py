import json
import requests

from .keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY


def get_weather_data(city, state):
    params = {
        "appid": OPEN_WEATHER_API_KEY,
        "q": f"{city},{state},US",
        "limit": 1,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct?"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    weather_response = requests.get(weather_url, params=params)
    weather_content = json.loads(weather_response.content)

    try:
        return{
            "temp": weather_content["main"]["temp"],
            "description": weather_content["weather"][0]["description"],
        }
    except (KeyError, IndexError):
        return None


# picture_url = content["photos"][0]["src"]["original"]
def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": f"{city} {state}",
        "per_page": 1,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        photo_url = content["photos"][0]["src"]["original"]
        return {"picture_url": photo_url}
    except (KeyError, IndexError):
        return {"picture_url": None}
